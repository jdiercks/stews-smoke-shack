<?php

    session_start();
    include_once('inc/conn.php');

    $username = $password = $usernameErr = $passwordErr = "";

    if(isset($_POST['login'])){
        
        $username = !empty($_POST['username']) ? trim($_POST['username']) : null;
        $password = !empty($_POST['password']) ? trim($_POST['password']) : null;

        $sql = "SELECT id, username, password FROM stews_login WHERE username = :username AND password = :password";
        $stmt = $pdo->prepare($sql);

        $stmt->bindValue(':username', $username);
        $stmt->bindValue(':password', $password);
        
        $stmt->execute();
        
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        
        if($user === false){

            echo 'Incorrect username / password combination!';

        }
        else{

            $validPassword = password_verify($password, $user['password']);

            if($validPassword == false){
                
                //if valid password then set user session id and location to admin page
                $_SESSION['user_id'] = $user['id'];
                $_SESSION['logged_in'] = time();
            
                header('Location: admin.php');
                exit;

            }else{
                //$validPassword was FALSE. Passwords do not match.
                die('Incorrect username / password combination!');
            }
            
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin Login</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/stews.css">
</head>
<body>
    <div class="main_pic">
        <img src="images/nav-logo.png" alt="logo">
    </div>
    <div class="login_form">
        <form action="admin_login.php" method="POST">
            <?php echo $usernameErr . "</br>" ?>
            <input class="username-login" type="text" name="username" value="username">
            <?php echo "</br>" . $passwordErr . "</br>" ?>
            <input class="password-login" type="password" name="password" value="password"><br>
            <div class="submit">
                <button class="submit_btn" type="submit" name="login">Submit</button>
            </div>
        </form>
    </div>
</body>
</html>