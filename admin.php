<?php

require('./inc/conn.php');

session_start();

$sql = 'SELECT * FROM menu';
$stmt = $pdo->prepare($sql);
$stmt->execute();
$menu = $stmt->fetchAll(PDO::FETCH_OBJ);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <link rel="stylesheet" href="css/stews.css">
</head>
<body>

    <nav>
        <ul>
            <li>
                <a href="./admin.php">Home</a>
            </li>
            <li>
                <a href="./inc/menu_insert.php">Create a Menu Item</a>
            </li>
            <li>
                <a href="./inc/admin_logout.php">Logout</a>
            </li>
        </ul>
    </nav>        

    <div class="table_menu">
        <table>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Picture</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
            <?php 
                foreach($menu as $item):
            ?>
            <tr>
                <form action="admin.php" method="POST">
                    <td><?= $item->menu_name ?></td>
                    <td><?= $item->menu_description ?></td>
                    <td><img class="menu_picture" src="uploads/<?= $item->menu_picture ?>" alt=""></td>
                    <td><?="$" . number_format($item->menu_price, 2) ?></td>
                    <td>
                        <button type="submit" name="update">
                            <a href="./inc/edit.php?id=<?= $item->menu_id ?>">
                                Edit
                            </a>
                        </button>    
                        <button type="submit" name="delete" value="delete">
                            <a onclick="return confirm('Are you sure you want to delete this menu item?')" href="./inc/delete.php?id=<?= $item->menu_id ?>">
                                Delete
                            </a>
                        </button>    
                    </td>
                </form>
            </tr>
            <?php 
                endforeach
            ?>
        </table>
    </div>

</body>
</html>