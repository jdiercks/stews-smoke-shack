<?php

require('test_conn.php');

session_start();

$sql = 'SELECT * FROM test';
$stmt = $pdo->prepare($sql);
$stmt->execute();
$menu = $stmt->fetchAll(PDO::FETCH_OBJ);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <style>
    
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

        .table_menu{
            display: flex;
            justify-content: center;
            align-items: center;
        }

        td a{
            text-decoration: none;
        }
        
        nav{
            background-color: #dddddd;
            display: flex;
            flex-direction: row;
        }

        nav ul{
            display: flex;
            flex-direction: row;
        }

        nav ul li{
            list-style-type: none;
        }

        nav ul li:nth-child(2){
            list-style-type: none;
            margin-left:50px;
        }

        nav ul li a{
            list-style-type: none;
            text-decoration: none;
            color: black;
            font-size: 18px;
        }

        .menu_picture{
            width: 125px;
            height: 125px;
        }

    </style>
</head>
<body>

    <nav>
        <ul>
            <li>
                <a href="./test_admin.php">Home</a>
            </li>
            <li>
                <a href="./test_insert.php">Create a Menu Item</a>
            </li>
        </ul>
    </nav>        

    <div class="table_menu">
        <table>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Picture</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
            <?php 
                foreach($menu as $item):
            ?>
            <tr>
                <form action="test_admin.php" method="POST">
                    <td><?= $item->menu_name ?></td>
                    <td><?= $item->menu_description ?></td>
                    <td><img class="menu_picture" src="uploads/<?= $item->menu_picture ?>" alt=""></td>
                    <td><?= $item->menu_price ?></td>
                    <td>
                        <button type="submit" name="update">
                            <a href="edit.php?id=<?= $item->menu_id ?>">
                                Edit
                            </a>
                        </button>    
                        <button type="submit" name="delete" value="delete">
                            <a onclick="return confirm('Are you sure you want to delete this menu item?')" href="delete.php?id=<?= $item->menu_id ?>">
                                Delete
                            </a>
                        </button>    
                    </td>
                </form>
            </tr>
            <?php 
                endforeach
            ?>
        </table>
    </div>

</body>
</html>