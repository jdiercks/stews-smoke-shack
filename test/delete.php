<?php

    require 'test_conn.php';

    $id = $_GET['id'];
    $sql = 'DELETE FROM test WHERE menu_id=:menu_id';
    $stmt = $pdo->prepare($sql);

    if($stmt->execute([':menu_id' => $id])){
        header('Location: test_admin.php');
    }


?>