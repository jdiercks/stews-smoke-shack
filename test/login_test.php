<?php

    session_start();
    include_once('test_conn.php');
    require('test_input.php');

    $admin = array($username, $password);

    $username = $password = $usernameErr = $passwordErr = "";

    if(isset($_POST['login'])){
        
        $username = !empty($_POST['username']) ? trim($_POST['username']) : null;
        $password = !empty($_POST['password']) ? trim($_POST['password']) : null;

        $admin = array($username, $password);

        $sql = "SELECT id, username, password FROM test_login WHERE username = :username";
        $stmt = $pdo->prepare($sql);

        foreach($admin as $value){
            echo "</br> $value </br>";
        }

        $stmt->bindValue(':username', $username);
        
        $stmt->execute();
        
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        
        if($user === false){

            echo 'Incorrect username / password combination!';

        }
        else{

            $validPassword = password_verify($password, $user['password']);

            if($validPassword === false){
                //need to figure out why this is not submitting when in an if statement
                $_SESSION['user_id'] = $user['id'];
                $_SESSION['logged_in'] = time();
            
                header('Location: test_admin.php');
                exit;

            }else{
                //$validPassword was FALSE. Passwords do not match.
                die('Incorrect username / password combination!');
            }
            
        }
    }
    

    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="login_test.php" method="POST">
        <?php echo $usernameErr . "</br>" ?>
        <input type="text" name="username" value="username">
        <?php echo "</br>" . $passwordErr . "</br>" ?>
        <input type="password" name="password" value="password">
        <button type="submit" name="login">Submit</button>
    </form>
</body>
</html>