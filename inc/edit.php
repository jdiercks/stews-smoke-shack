<?php
    require('conn.php');
    $id = $_GET['id'];

    $sql = 'SELECT * FROM menu WHERE menu_id=:menu_id';
    $stmt = $pdo->prepare($sql);
    $stmt->execute([':menu_id' => $id]);
    $menu = $stmt->fetch(PDO::FETCH_OBJ);

    if(isset($_POST['edit'])){
        try {
            echo "edit was clicked";            

                $name = $_POST['name'];
                $description = $_POST['description'];
                $picture = $_FILES['picture'];
                $price = $_POST['price'];

                // setlocale(LC_MONETARY, 'en_US.UTF-8');
                // money_format('%.2n', $price);

                $pictureName = $_FILES['picture']['name'];
                $pictureTmpName = $_FILES['picture']['tmp_name'];
                $pictureSize = $_FILES['picture']['size'];
                $pictureError = $_FILES['picture']['error'];

                $upload_dir = '../uploads/';
                $pictureActualExt = strtolower(pathinfo($pictureName, PATHINFO_EXTENSION));
                $allowed = ['jpeg', 'jpg', 'png', 'gif', 'pdf'];
                $picMenu = rand(1000, 1000000).".".$pictureActualExt;

                move_uploaded_file($pictureTmpName, $upload_dir.$picMenu);

                $stmt = $pdo->prepare('UPDATE menu SET menu_name=:menu_name, menu_description=:menu_description, menu_picture=:menu_picture, menu_price=:menu_price WHERE menu_id=:menu_id');         

                if($stmt->execute([':menu_name' => $name, ':menu_description' => $description, ':menu_picture' => $picMenu, ':menu_price' => $price, ':menu_id' => $id])){
                    header('Location: ../admin.php');
                    echo "this was done right";
                }

            

            //make some message using if statement to let user know record was inserted
        }
        catch(PDOException $e)
        {
            echo $sql . "<br>" . $e->getMessage();
        }
        
        $pdo = null;

    }   
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Stew's Edit</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/stews.css">
    <style>
    
    nav{
            background-color: #dddddd;
            display: flex;
            flex-direction: row;
        }

        nav ul{
            display: flex;
            flex-direction: row;
        }

        nav ul li{
            list-style-type: none;
        }

        nav ul li:nth-child(2){
            list-style-type: none;
            margin-left:50px;
        }

        nav ul li a{
            list-style-type: none;
            text-decoration: none;
            color: black;
            font-size: 18px;
        }

        .menu_picture{
            width: 125px;
            height: 125px;
        }
    
    </style>
</head>
<body>
    
    <nav>
        <ul>
            <li>
                <a href="../admin.php">Home</a>
            </li>
            <li>
                <a href="./menu_insert.php">Create a Menu Item</a>
            </li>
        </ul>
    </nav> 

    <form method="POST" enctype="multipart/form-data">
        <label for="">Name</label>
        <input type="text" name="name" value="<?= $menu->menu_name; ?>">
        <label for="">Description</label>
        <input type="text" name="description" value="<?= $menu->menu_description; ?>">
        <label for="">Picture</label>
        <img class="menu_picture" src="../uploads/<?= $menu->menu_picture; ?>" alt="uploaded image">
        <input type="file" name="picture" value="">
        <label for="">Price</label>
        <input type="text" name="price" value="<?= $menu->menu_price; ?>">
        <button type="submit" name="edit">Edit</button>
    </form>

</body>
</html>