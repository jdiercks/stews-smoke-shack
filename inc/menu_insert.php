<?php
    require('conn.php');
    $message = '';
    
    if(isset($_POST['insert'])){
        try {
            if(empty($_POST['name']) || empty($_POST['description']) || empty($_POST['price'])){
                
                $message = 'Please enter a value';

            }else{

                $name = $_POST['name'];
                $description = $_POST['description'];
                $picture = $_FILES['picture'];
                $price = $_POST['price'];

                

                $pictureName = $_FILES['picture']['name'];
                $pictureTmpName = $_FILES['picture']['tmp_name'];
                $pictureSize = $_FILES['picture']['size'];
                $pictureError = $_FILES['picture']['error'];

                $upload_dir = '../uploads/';
                $pictureActualExt = strtolower(pathinfo($pictureName, PATHINFO_EXTENSION));
                $allowed = ['jpeg', 'jpg', 'png', 'gif', 'pdf'];
                $picMenu = rand(1000, 1000000).".".$pictureActualExt;
                

                if(in_array($pictureActualExt, $allowed)){

                    if($pictureError === 0){

                        if($pictureSize < 1000000){
                            
                            $pictureNameNew = uniqid('', true).".".$pictureActualExt;
                            $pictureDestination = 'upload/'.$pictureNameNew;
                            move_uploaded_file($pictureTmpName, $upload_dir.$picMenu);

                        }else{
                            $message = 'your file is to big';
                        }

                    }else{
                        $message = 'there was an error uploading your file';
                    }

                }else{
                    $message = 'You cannot upload files of this type';
                }

                $stmt = $pdo->prepare("INSERT INTO menu (menu_name, menu_description, menu_picture, menu_price) 
                VALUES (:menu_name, :menu_description, :menu_picture, :menu_price)");

                $stmt->execute([':menu_name' => $name, ':menu_description' => $description, ':menu_picture' => $picMenu, ':menu_price' => $price]);
                $message = 'Menu item entered successfully';

                // setlocale(LC_MONETARY, 'en_US.UTF-8');
                // money_format('%.2n', $price);
            }
            

            //make some message using if statement to let user know record was inserted
        }
        catch(PDOException $e)
        {
            echo $sql . "<br>" . $e->getMessage();
        }
        
        $pdo = null;

    }           

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
    
    .message{
        background-color: green;
    }

    nav{
        background-color: #dddddd;
        display: flex;
        flex-direction: row;
    }

    nav ul{
        display: flex;
        flex-direction: row;
    }

    nav ul li{
        list-style-type: none;
    }

    nav ul li:nth-child(2){
        list-style-type: none;
        margin-left:50px;
    }

    nav ul li a{
        list-style-type: none;
        text-decoration: none;
        color: black;
        font-size: 18px;
    }
    
    </style>
</head>
<body>

    <nav>
        <ul>
            <li>
                <a href="../admin.php">Home</a>
            </li>
            <li>
                <a href="./menu_insert.php">Create a Menu Item</a>
            </li>
        </ul>
    </nav> 

    <?php if(!empty($message)): ?>
        <div class="message">
            <?php echo $message ?>
        </div>
    <?php endif; ?>

    <form method="POST" enctype="multipart/form-data">
        <label for="">Name</label>
        <input type="text" name="name" value="">
        <label for="">Description</label>
        <input type="text" name="description" value="">
        <label for="">Picture</label>
        <input type='file' name="picture" />
        <label for="">Price</label>
        <input type="text" name="price" value="">
        <button type="submit" name="insert">Add</button>
    </form>

</body>
</html>