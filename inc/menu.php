<?php

    class Menu {

        public function fetch_all(){
            global $pdo;

            $query = $pdo->prepare("SELECT * FROM menu");
            $query->execute();

            return $query->fetchAll();
        }

    }

?>