<?php

    require 'conn.php';

    $id = $_GET['id'];
    $sql = 'DELETE FROM menu WHERE menu_id=:menu_id';
    $stmt = $pdo->prepare($sql);

    if($stmt->execute([':menu_id' => $id])){
        header('Location: ../admin.php');
    }


?>